from typing import List
from pydrive.drive import GoogleDrive 
from pydrive.auth import GoogleAuth 
from collections import namedtuple
import os


FileObject = namedtuple('FileObject', ('title', 'is_folder', 'id', 'parent_id', 'gdfo'))
mimeTypeFolder = 'application/vnd.google-apps.folder'


class GoogleDriveUpload:
	def __init__(self):
		g_auth = GoogleAuth(settings_file='credentials/settings.yaml')
		g_auth.LocalWebserverAuth()
		self._drive = GoogleDrive(g_auth)
		self._root_object_id = None

	def _get_file_object(self, _object):
		return FileObject(
			title=_object['title'], 
			is_folder=_object['mimeType'] == mimeTypeFolder, 
			id=_object['id'],
			parent_id=_object['parents'][0]['id'],
			gdfo=_object
		)

	
	def get_root_object_id(self) -> str:
		"""
		Get id of root folder

		Returns:
			str: id of root folder
		"""
		if not self._root_object_id:
			object_list = self.get_object_list(max_results=1)
			if object_list:
				self._root_object_id = object_list[0].parent_id

		return self._root_object_id

	def get_object_list(
		self, 
		folder_name: str = None, 
		object_name: str = None, 
		max_results: int = None
	) -> List[FileObject]:
		""" 
		Get files and folder in google drive folder

		Args:
			folder_name (str, optional): folder name. Defaults to None for root folder.
			object_name (str, optional): file or folder name. Defaults to None for all files and folders.
			max_results (str, optional): maximum count of result entries. Defaults to None for all results.

		Returns:
			List[FileObject]: list of FileObject objects
		"""
		query = {}

		if folder_name and (_objects := self.get_object_list(folder_name=None, object_name=folder_name)):
			folder_id = _objects[0].id
		else:
			folder_id = 'root'

		query['q'] = f"'{folder_id}' in parents and trashed=false"
		if object_name:
			query['q'] = f"title = '{object_name}' and " + query['q']
		if max_results:
			query['maxResults'] = max_results

		file_object_list = []
		object_list = self._drive.ListFile(query).GetList()
		for _object in object_list:
			file_object = self._get_file_object(_object)
			file_object_list.append(file_object)

		return file_object_list
	
	def create_folder_if_not_exists(self, folder_name: str) -> FileObject:
		"""
		Create remote folder if not exists

		Args:
			folder_name (str): remote folder name

		Returns:
			FileObject: FileObject for existing or created folder
		"""
		if (_objects := self.get_object_list(object_name=folder_name)) and _objects[0].is_folder:
			return _objects[0]
		else:
			folder = self._drive.CreateFile({
				'mimeType': 'application/vnd.google-apps.folder',
				'title': folder_name
			})
			folder.Upload()
			return self._get_file_object(folder)

	def delete_file_if_exists(self, file_name: str, folder_name: str = None):
		_objects = self.get_object_list(folder_name=folder_name, object_name=file_name)
		for _object in _objects:
			_object.gdfo.Trash()

	def upload_file_to_folder(
		self, 
		local_folder_name: str, 
		file_name: str, 
		remote_folder_name: str = None,
		delete_if_exists: bool = True
	) -> None:
		"""
		Upload file to specific remote folder

		Args:
			local_folder_name (str): local path
			file_name (str): local file name (remote will be same)
			remote_folder_name (str, optional): remote folder name. Defaults to None for root folder.
			delete_if_exists (bool, optional): delete files if exists. Defaults to True.
		"""
		if remote_folder_name:
			folder_id = self.create_folder_if_not_exists(remote_folder_name).id
		else:
			folder_id = self.get_root_object_id()
		
		if delete_if_exists:
			self.delete_file_if_exists(file_name=file_name, folder_name=remote_folder_name)

		query = {
			'parents': [{'id': folder_id}],
			'title': file_name
		}
		f = self._drive.CreateFile(query) 
		f.SetContentFile(os.path.join(local_folder_name, file_name)) 
		f.Upload() 
		f = None
