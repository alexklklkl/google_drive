# Upload local file to specific folder on google drive

## Example:
```
drive = GoogleDriveUpload()
drive.upload_file_to_folder(local_folder_name, file_name, remote_folder_name)
```
## Steps:
- install requirements.txt:
    - pip install -r requirements.txt
- obtain **client_id** and **client_secret**:
    - https://pythonhosted.org/PyDrive/quickstart.html (five steps),
    - rename *credentials/settings_example.yaml* to *credentials/settings.yaml*
    - in *credentials/settings.yaml* fill **client_id** and **client_secret** with obtained values
